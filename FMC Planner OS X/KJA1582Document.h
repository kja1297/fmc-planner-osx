//
//  KJA1582Document.h
//  FMC Planner OS X
//
//  Created by Kilian Hofmann on 09.03.14.
//  Copyright (c) 2014 Kilian Hofmann. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface KJA1582Document : NSDocument <NSMenuDelegate, NSApplicationDelegate>
{
    IBOutlet NSTextField *Aircraft;
    IBOutlet NSTextField *Units;
    IBOutlet NSTextField *Flightnumber;
    
    IBOutlet NSTextField *Dep_arpt;
    IBOutlet NSTextField *Dep_rwy;
    IBOutlet NSTextField *Dep_SID;
    IBOutlet NSTextField *Arr_arpt;
    IBOutlet NSTextField *Arr_rwy;
    IBOutlet NSTextField *Arr_trans;
    IBOutlet NSTextField *Arr_STAR;
    
    IBOutlet NSTextField *zfw;
    IBOutlet NSTextField *gw;
    IBOutlet NSTextField *pax;
    IBOutlet NSTextField *fuel;
    IBOutlet NSTextField *reserve;
    IBOutlet NSTextField *crz_alt;
    IBOutlet NSTextField *trans_alt;
    IBOutlet NSTextField *cost_index;
    IBOutlet NSTextField *flaps;
    IBOutlet NSTextField *v1;
    IBOutlet NSTextField *v2;
    IBOutlet NSTextField *vr;
    
    IBOutlet NSTextField *Route;

    NSArray *FileContents;
    bool readSuccess;
    bool close;
}

@end
