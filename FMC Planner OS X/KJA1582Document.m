//
//  KJA1582Document.m
//  FMC Planner OS X
//
//  Created by Kilian Hofmann on 09.03.14.
//  Copyright (c) 2014 Kilian Hofmann. All rights reserved.
//

#import "KJA1582Document.h"

@implementation KJA1582Document

- (id)init
{
    self = [super init];
    if (self)
    {
        // Add your subclass-specific initialization here.
    }
    return self;
}

- (NSString *)windowNibName
{
    // Override returning the nib file name of the document
    // If you need to use a subclass of NSWindowController or if your document supports multiple NSWindowControllers, you should remove this method and override -makeWindowControllers instead.
    return @"KJA1582Document";
}

-(void)awakeFromNib
{
    if (readSuccess)
    {
        [Aircraft setStringValue:[FileContents objectAtIndex:0]];
        [Units setStringValue:[FileContents objectAtIndex:1]];
        [Flightnumber setStringValue:[FileContents objectAtIndex:2]];
        [Dep_arpt setStringValue:[FileContents objectAtIndex:3]];
        [Dep_rwy setStringValue:[FileContents objectAtIndex:4]];
        [Dep_SID setStringValue:[FileContents objectAtIndex:5]];
        [Arr_arpt setStringValue:[FileContents objectAtIndex:6]];
        [Arr_rwy setStringValue:[FileContents objectAtIndex:7]];
        [Arr_STAR setStringValue:[FileContents objectAtIndex:8]];
        [Arr_trans setStringValue:[FileContents objectAtIndex:9]];
        [pax setStringValue:[FileContents objectAtIndex:10]];
        [fuel setStringValue:[FileContents objectAtIndex:11]];
        [reserve setStringValue:[FileContents objectAtIndex:12]];
        [zfw setStringValue:[FileContents objectAtIndex:13]];
        [gw setStringValue:[FileContents objectAtIndex:14]];
        [cost_index setStringValue:[FileContents objectAtIndex:15]];
        [flaps setStringValue:[FileContents objectAtIndex:16]];
        [crz_alt setStringValue:[FileContents objectAtIndex:17]];
        [trans_alt setStringValue:[FileContents objectAtIndex:18]];
        [Route setStringValue:[FileContents objectAtIndex:19]];
        [v1 setStringValue:[FileContents objectAtIndex:20]];
        [vr setStringValue:[FileContents objectAtIndex:21]];
        [v2 setStringValue:[FileContents objectAtIndex:22]];
    }
}

- (BOOL)windowShouldClose:(id)sender
{
    [self displayAlert];
    if (close)
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

- (NSApplicationTerminateReply)applicationShouldTerminate:(NSApplication *)sender
{
    [self displayAlert];
    if (close)
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

-(void)displayAlert
{
    NSAlert *alert = [[NSAlert alloc] init];
    [alert setMessageText:@"Warning!"];
    [alert setInformativeText:@"If you continue, all unsaved data will be lost."];
    
    [alert addButtonWithTitle:@"Ok"];
    [alert addButtonWithTitle:@"Abort"];
    
    [alert beginSheetModalForWindow:[self windowForSheet]
                      modalDelegate:self
                     didEndSelector:@selector(alertDidEnd:returnCode:contextInfo:)
                        contextInfo:nil];
    [NSApp runModalForWindow:[self windowForSheet]];
}

- (void)alertDidEnd:(NSAlert *)a returnCode:(NSInteger)rc contextInfo:(void *)ci
{
    close = NO;
    switch(rc)
    {
        case NSAlertFirstButtonReturn:
            // "First" pressed
            [NSApp stopModal];
            close = YES;
            break;
        case NSAlertSecondButtonReturn:
            // "Second" pressed
            [NSApp stopModal];
            close = NO;
            break;
    }
}

- (void)windowControllerDidLoadNib:(NSWindowController *)aController
{
    [super windowControllerDidLoadNib:aController];
    // Add any code here that needs to be executed once the windowController has loaded the document's window.
}

-(NSArray *)save
{
    NSArray *dataFromView = [[NSArray alloc]initWithObjects:[Aircraft stringValue], [Units stringValue], [Flightnumber stringValue],[Dep_arpt stringValue],[Dep_rwy stringValue],[Dep_SID stringValue],[Arr_arpt stringValue],[Arr_rwy stringValue],[Arr_STAR stringValue],[Arr_trans stringValue],[pax stringValue],[fuel stringValue],[reserve stringValue],[zfw stringValue],[gw stringValue],[cost_index stringValue],[flaps stringValue],[crz_alt stringValue],[trans_alt stringValue],[Route stringValue],[v1 stringValue],[vr stringValue],[v2 stringValue], nil];
    return dataFromView;
}

- (NSData *)dataOfType:(NSString *)typeName error:(NSError **)outError
{
//    // Insert code here to write your document to data of the specified type. If outError != NULL, ensure that you create and set an appropriate error when returning nil.
//    // You can also choose to override -fileWrapperOfType:error:, -writeToURL:ofType:error:, or -writeToURL:ofType:forSaveOperation:originalContentsURL:error: instead.
//    NSException *exception = [NSException exceptionWithName:@"UnimplementedMethod" reason:[NSString stringWithFormat:@"%@ is unimplemented", NSStringFromSelector(_cmd)] userInfo:nil];
//    @throw exception;
//    return nil;
    
    NSData *data;
    data = [NSPropertyListSerialization dataFromPropertyList:[self save] format:NSPropertyListBinaryFormat_v1_0 errorDescription:nil];
    if (!data && outError) {
        *outError = [NSError errorWithDomain:NSCocoaErrorDomain
                                        code:NSFileWriteUnknownError userInfo:nil];
    }
    
    return data;
}

- (BOOL)readFromData:(NSData *)data ofType:(NSString *)typeName error:(NSError **)outError
{
    /*
    // Insert code here to read your document from the given data of the specified type. If outError != NULL, ensure that you create and set an appropriate error when returning NO.
    // You can also choose to override -readFromFileWrapper:ofType:error: or -readFromURL:ofType:error: instead.
    // If you override either of these, you should also override -isEntireFileLoaded to return NO if the contents are lazily loaded.
    NSException *exception = [NSException exceptionWithName:@"UnimplementedMethod" reason:[NSString stringWithFormat:@"%@ is unimplemented", NSStringFromSelector(_cmd)] userInfo:nil];
    @throw exception;
    return YES;*/
    
    readSuccess = NO;
    NSArray *fileContents = [[NSArray alloc] init];
    fileContents = [NSPropertyListSerialization propertyListFromData:data mutabilityOption:NSPropertyListImmutable format:nil errorDescription:nil];
    if (!fileContents && outError)
    {
        *outError = [NSError errorWithDomain:NSCocoaErrorDomain
                                        code:NSFileReadUnknownError userInfo:nil];
    }
    if (fileContents)
    {
        readSuccess = YES;
        FileContents = [[NSArray alloc]initWithArray:fileContents copyItems:YES];
    }
    return readSuccess;
}

@end
