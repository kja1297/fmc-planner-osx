//
//  KJA1582AppDelegate.h
//  FMC Planner OS X
//
//  Created by Kilian Hofmann on 09.03.14.
//  Copyright (c) 2014 Kilian Hofmann. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface KJA1582AppDelegate : NSObject <NSApplicationDelegate>

@end
