//
//  KJA1582AppDelegate.m
//  FMC Planner OS X
//
//  Created by Kilian Hofmann on 09.03.14.
//  Copyright (c) 2014 Kilian Hofmann. All rights reserved.
//

#import "KJA1582AppDelegate.h"

@implementation KJA1582AppDelegate

- (NSApplicationTerminateReply)applicationShouldTerminate:(NSApplication *)sender
{
    NSAlert *alert = [[NSAlert alloc] init];
    [alert setMessageText:@"Warning!"];
    [alert setInformativeText:@"If you continue, all unsaved data will be lost."];
    
    [alert addButtonWithTitle:@"Proceed"];
    [alert addButtonWithTitle:@"Abort"];
    
    if ([alert runModal] == NSAlertFirstButtonReturn)
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

@end
