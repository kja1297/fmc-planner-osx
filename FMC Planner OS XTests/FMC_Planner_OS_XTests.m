//
//  FMC_Planner_OS_XTests.m
//  FMC Planner OS XTests
//
//  Created by Kilian Hofmann on 09.03.14.
//  Copyright (c) 2014 Kilian Hofmann. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface FMC_Planner_OS_XTests : XCTestCase

@end

@implementation FMC_Planner_OS_XTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample
{
    XCTFail(@"No implementation for \"%s\"", __PRETTY_FUNCTION__);
}

@end
